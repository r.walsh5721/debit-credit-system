import Account
import Card
import os.path
from os import path

class Interface:
    acc = Account.Account()
    card = Card.Card()

    # Function to check if value enter is not empty
    def validInput(self, word):
        if word == '':
            print("Please enter a word")
            self.createAccMenu()

    # Transaction menu for Debit
    def transacMenu(self):
        # Checks the length so it does not add the transactions twice to the array
        if len(self.acc.transactions) == 0:
            self.acc.addDebTransactions(self.card.accountNo)
        else:
            pass

        print("Debit Transations Menu:")
        print("1: Transation History")
        print("2: Amount of Transations")
        print("3: Add to Start of Transactions")
        print("4: Insert Transaction")
        print("5: Delete First Transaction")
        print("6: Delete Last Transation")
        print("7: Does Value Exist")
        print("8: Get Value from Index")
        print("9: Beginning Insert Test")
        print("10: End Insert Test")
        print("11: Exit")
        input1 = input()
        if input1 == "1":
            print("Transaction History (Transactions - Balance + Overdraft):")
            self.acc.getTrans("Debit")
            self.transacMenu()       
        elif input1 == "2":
            print("Amount of Transactions:",len(self.acc.transactions))
            self.transacMenu()
        elif input1 == "3":
            print("Enter Value:")
            input1 = input()
            if input1.isdigit():
                self.acc.addFirstTrans("Debit",input1)
            else:
                print("Please enter a number")
                self.transacMenu()
        elif input1 == "4":
            print("Current Transaction History:")
            self.acc.getTrans("Debit")
            print("Enter Index:")
            input1 = input()
            print("Enter Value:")
            input2 = input()
            if input1.isdigit() and input2.isdigit():
                self.acc.insertTrans(input1,input2)
            else:
                print("Please enter a number for both values")
                self.transacMenu()
        elif input1 == "5":
            self.acc.removeFirstTrans("Debit")
            self.transacMenu()
        elif input1 == "6":
            self.acc.removeLastTrans("Debit")
            self.transacMenu()
        elif input1 == "7":
            print("Enter Value:")
            input1 = input()
            if input1.isdigit():
                self.acc.itemExist("Debit",int(input1))
        elif input1 == "8":
            print("Enter Index:")
            input1 = input()
            if input1.isdigit():
                self.acc.getItem("Debit",int(input1))
        elif input1 == "9":
            self.acc.beginningInsertTest("Debit")
        elif input1 == "10":
            self.acc.endInsertTest("Debit")
        elif input1 == "11":
            print("Application Closed")
            exit()

    # Transaction menu for credit
    def creTransacMenu(self):
        if len(self.acc.creTransactions) == 0:
            self.acc.addCreTransactions(self.card.accountNo)

        print("Credit Transations Menu:")
        print("1: Transation History")
        print("2: Amount of Transations")
        print("3: Add to Start of Transactions")
        print("4: Add to End of Transactions")
        print("5: Delete First Transaction")
        print("6: Delete Last Transation")
        print("7: Does Value Exist")
        print("8: Get Value from Index")
        print("9: Beginning Insert Test")
        print("10: End Insert Test")
        print("11: Exit")
        input1 = input()
        if input1 == "1":
            print("Transaction History (Transactions - Balance + Overdraft):")
            self.acc.getTrans("Credit")
            self.creTransacMenu()
        elif input1 == "2":
            print("Amount of Transactions:",len(self.acc.creTransactions))
            self.creTransacMenu()
        elif input1 == "3":
            print("Enter Value:")
            input1 = input()
            if input1.isdigit():
                self.acc.addFirstTrans("Credit",int(input1))
            else:
                print("Please enter a number")
                self.creTransacMenu()
        elif input1 == "4":
            print("Enter Value:")
            input1 = input()
            self.acc.getTrans("Credit")

            if input1.isdigit():
                self.acc.addLastCreTransac(input1)
            else:
                print("Please enter a number for both values")
                self.creTransacMenu()
        elif input1 == "5":
            self.acc.removeFirstTrans("Credit")
            self.creTransacMenu()
        elif input1 == "6":
            self.acc.removeLastTrans("Credit")
            self.creTransacMenu()
        elif input1 == "7":
            print("Enter Value:")
            input1 = input()
            if input1.isdigit():
                self.acc.itemExist("Credit",int(input1))
        elif input1 == "8":
            print("Enter Index:")
            input1 = input()
            if input1.isdigit():
                self.acc.getItem("Credit",int(input1))
        elif input1 == "9":
            self.acc.beginningInsertTest("Credit")
        elif input1 == "10":
            self.acc.endInsertTest("Credit")
        elif input1 == "11":
            print("Application Closed")
            exit()

    # Debit Menu
    def debitCardMenu(self):
        print("Debit Account:\n")
        print("1: See Balance")
        print("2: Add Funds")
        print("3: Charge Account")
        print("4: Transaction Menu")
        print("5: Exit")
        input1 = input()
        if input1 == "1":
            self.card.getOverallBalance("Debit")
            self.debitCardMenu()
        elif input1 == "2":
            print("How much are you adding?")
            print("Amount: ")
            input1 = input()
            self.card.editFunds("+","Debit", int(input1))
        elif input1 == "3":
            print("How much are you charging?")
            print("Amount:")
            input1 = input()
            self.card.editFunds("-","Debit", int(input1))
        elif input1 == "4":
            self.transacMenu()
        elif input1 == "5":
            print("Application Closed")
            exit()

    # Credit Menu
    def creditCardMenu(self):
        print("Credit Account:\n")
        print("1: See Balance")
        print("2: Add Funds")
        print("3: Charge Account")
        print("4: Transaction Menu")
        print("5: Exit")
        input1 = input()
        if input1 == "1":
            self.card.getOverallBalance("Credit")
        elif input1 == "2":
            print("How much are you adding?")
            print("Amount: ")
            input1 = input()
            self.card.editFunds("+","Credit", int(input1))
        elif input1 == "3":
            print("How much are you charging?")
            print("Amount:")
            input1 = input()
            self.card.editFunds("-","Credit", int(input1))
        elif input1 == "4":
            self.creTransacMenu()
        elif input1 == "5":
            print("Application Closed")
            exit()
            
    # Gives account details to the user (Option 3 of Main Menu)
    def accountMenu(self):
        fNames = self.acc.retrieveOtherData("FName")
        lNames = self.acc.retrieveOtherData("LName")
        dates = self.acc.retrieveOtherData("DoB")
        print("Account:")
        print("First Name: ", fNames[self.card.accountNo])
        print("Last Name: ", lNames[self.card.accountNo])
        print("Date of Birth: ", dates[self.card.accountNo])

    # Menu shown after logging in
    def mainMenu(self):
        print("Welcome")
        print("Main Menu:")
        print("1: Debit Card")
        print("2: Credit Card")
        print("3: Account Details")
        print("4: Exit")
        input1 = input()
        if input1 == "1":
            self.debitCardMenu()
        elif input1 == "2":
            self.creditCardMenu()
        elif input1 == "3":
            self.accountMenu()
        elif input1 == "4":
            print("Application Closed")
            exit()

    # Menu for Login
    def loginMenu(self):
        users = self.acc.retrieveLoginData("Users")
        passwords = self.acc.retrieveLoginData("Passwords")
        #print(users)
        #print(passwords)
        print("Login")
        print("Username: ")
        usern = input()
        for i in range(0,len(users)):
            if usern == users[i]:
                print("Password: ")
                passw = input()
                if passw == passwords[i]:
                    self.card.accountNo = i
                    self.acc.accountNo = i
                    self.mainMenu()
        
    # Menu for Account Creation
    def createAccMenu(self):
        if path.exists("Login_Data.data"):
            users = self.acc.retrieveLoginData("Users")
            if len(users) == 0:
                num = 0
            else:
                num = len(users)
            self.acc.setAccountNo(str(num))
            print("Create Account - Make sure all details are filled in")
            print("Enter Username: ")
            input1 = input()
            self.validInput(input1)
            for i in range(0,len(users)):
                if input1 == users[i]:
                    print("Username already in use please enter a different Username")
                    self.createAccMenu()

            self.acc.setUserName(input1)
            print("Enter Password: ")
            input1 = input()
            self.validInput(input1)
            self.acc.setPassword(input1)
            print("Enter First Name: ")
            self.validInput(input1)
            input1 = input()
            self.acc.setFName(input1)
            print("Enter Last Name: ")
            self.validInput(input1)
            input1 = input()
            self.acc.setLName(input1)
            print("Enter Date of Birth: ")
            self.validInput(input1)
            input1 = input()
            self.acc.setDoB(input1)
            print("Enter Debit Balance: ")
            input1 = input()
            self.acc.setDebitBalance(input1)
            print("Enter Overdraft Limit: ")
            input1 = input()
            self.acc.setOverdraft(input1)
            print("Enter Credit Balance: ")
            input1 = input()
            self.acc.setCreditBalance(input1)
            self.acc.loginDatabase()
            self.acc.otherDatabase()
            self.acc.transacDatabase()
        else:
            f = open("Login_Data.data", "a+")
            print("Login_Data File Added. Reload Application to create account.")
            f.close()

    # Start menu
    def startMenu(self):
        i = Interface()
        print("Start Menu")
        print("1: Login")
        print("2: Create Account")
        input1 = input()
        if input1 == "1":
            i.loginMenu()
        elif input1 == "2":
            i.createAccMenu()
        else:
            print("Please enter a valid option.")

    '''
    Function that will check if the files needed have already been created.
    If not then Create the files needed.
    '''
    def fileCheck(self):
        if path.exists("Login_Data.data") and path.exists("Other_Details.data") and path.exists("Transactions.data"):
            pass
        else:
            print("Three files have been created to store data in your account area")
            f = open("Login_Data.data", "a+")
            f.close()
            f = open("Other_Details.data", "a+")
            f.close()
            f = open("Transactions.data", "a+")
            f.close()
        
        self.startMenu()

if __name__ == "__main__":
    i = Interface()
    i.fileCheck()