import numpy as np
import os.path
import ctypes
import re
import DynamicArray
from os import path

class Account:
    transactions = DynamicArray.DynamicArray()
    def __init__(self, accountNo=None, userName=None, password=None, fName=None, lName=None, doB=None, debitBalance=None, overdraft=None, creditBalance=None):
        self._accountNo = accountNo
        self._userName = userName
        self._password = password
        self._fName = fName
        self._lName = lName
        self._doB = doB
        self._debitBalance = debitBalance
        self._overdraft = overdraft
        self._creditBalance = creditBalance

    # Setter functions created for the Account details
    def setAccountNo(self, accountNo):
        self._accountNo = accountNo

    def setUserName(self, userName):
        self._userName = userName

    def setPassword(self, password):
        self._password = password

    def setFName(self, fName):
        self._fName = fName

    def setLName(self, lName):
        self._lName = lName

    def setDoB(self, doB):
        self._doB = doB

    def setDebitBalance(self, debitBalance):
        self._debitBalance = debitBalance

    def setOverdraft(self, overdraft):
        self._overdraft = overdraft

    def setCreditBalance(self, creditBalance):
        self._creditBalance = creditBalance

    # Getter functions created for the Account details
    def getAccountNo(self):
        return self._accountNo

    def getUserName(self):
        return self._userName

    def getPassword(self):
        return self._password

    def getFName(self):
        return self._fName

    def getLName(self):
        return self._lName

    def getDoB(self):
        return self._doB

    def getDebitBalance(self):
        return self._debitBalance

    def getOverdraft(self):
        return self._overdraft

    def getCreditBalance(self):
        return self._creditBalance

    # Adds login details to the file "Login_Data.data"
    def loginDatabase(self):
        usern = self.getUserName()
        passw = self.getPassword()
        f = open("Login_Data.data", "a+")
        f.write(usern + "\n")
        f.write(passw + "\n")
        f.close()

    # Add other details to the file "Other_Details.data"
    def otherDatabase(self):
        accNum = self.getAccountNo()
        fiName = self.getFName()
        laName = self.getLName()
        dob = self.getDoB()
        debitBal = self.getDebitBalance()
        overdraft = self.getOverdraft()
        creditBal = self.getCreditBalance()

        f = open("Other_Details.data", "a+")
        f.write(accNum + "\n")
        f.write(fiName + "\n")
        f.write(laName + "\n")
        f.write(dob + "\n")
        f.write(debitBal + "\n")
        f.write(overdraft + "\n")
        f.write(creditBal + "\n")
        f.close()

    def transacDatabase(self):
        accNum = self.getAccountNo()
        debitBal = self.getDebitBalance()
        overdraft = self.getOverdraft()
        creditBal = self.getCreditBalance()
        totalDebAmount = int(debitBal) + int(overdraft)
        totalCreAmount = int(creditBal) + int(overdraft)

        f = open("Transactions.data", "a+")
        f.write(str(accNum) + "\n")
        f.write(str(totalDebAmount) + "\n")
        f.write(str(totalCreAmount)+ "\n")
        f.close()

    def newTransaction(self, accNo, debAmount, creAmount):
        f = open("Transactions.data", "a+")
        f.write(str(accNo) + "\n")
        f.write(str(debAmount) + "\n")
        f.write(str(creAmount) + "\n")
        f.close()

    # Gets the usernames from the file "Login_Data.data"
    def retrieveUsernames(self):
        users = []
        f = open("Login_Data.data", "r")
        if f.mode == 'r':
            data = f.readlines()
            for x in data:
                x = x[:-1]
                users.append(x)
        users = users[0::2]
        return users

    # Gets the passwords from the file "Login_Data.data"
    def retrievePasswords(self):
        passwords = []
        f = open("Login_Data.data", "r")
        if f.mode == 'r':
            data = f.readlines()
            for x in data:
                x = x[:-1]
                passwords.append(x)
        passwords = passwords[1::2]
        return passwords

    # Gets the Account Numbers from the file "Other_Details.data"
    def retrieveAccountNos(self):
        dates = []
        if path.exists("Other_Details.data"):
            f = open("Other_Details.data", "r")
            if f.mode == 'r':
                data = f.readlines()
                for x in data:
                    x = x[:-1]
                    dates.append(x)
        dates = dates[0::7]
        return dates

    # Gets the First Names from the file "Other_Details.data"
    def retrieveFName(self):
        names = []
        if path.exists("Other_Details.data"):
            f = open("Other_Details.data", "r")
            if f.mode == 'r':
                data = f.readlines()
                for x in data:
                    x = x[:-1]
                    names.append(x)
        names = names[1::7]
        return names

    # Gets the Last Names from the file "Other_Details.data"
    def retrieveLName(self):
        names = []
        if path.exists("Other_Details.data"):
            f = open("Other_Details.data", "r")
            if f.mode == 'r':
                data = f.readlines()
                for x in data:
                    x = x[:-1]
                    names.append(x)
        names = names[2::7]
        return names

    # Gets the Date of Births from the file "Other_Details.data"
    def retrieveDoB(self):
        dates = []
        if path.exists("Other_Details.data"):
            f = open("Other_Details.data", "r")
            if f.mode == 'r':
                data = f.readlines()
                for x in data:
                    x = x[:-1]
                    dates.append(x)
        dates = dates[3::7]
        return dates

    # Gets the Debitbalances from the file "Other_Details.data"
    def retrieveDebitDetails(self):
        balances = []
        if path.exists("Other_Details.data"):
            f = open("Other_Details.data", "r")
            if f.mode == "r":
                data = f.readlines()
                for x in data:
                    x = x[:-1]
                    balances.append(x)
        #Gets the 5th element and every 7th element after that eg index 4,10...
        balances = balances[4::7]
        return balances

    def retrieveOverdraftDetails(self):
        balances = []
        if path.exists("Other_Details.data"):
            f = open("Other_Details.data", "r")
            if f.mode == "r":
                data = f.readlines()
                for x in data:
                    x = x[:-1]
                    balances.append(x)
        #Gets the 6th element and every 7th element after that eg index 5,11...
        balances = balances[5::7]
        return balances

    def retrieveCreditDetails(self):
        balances = []
        if path.exists("Other_Details.data"):
            f = open("Other_Details.data", "r")
            if f.mode == "r":
                data = f.readlines()
                for x in data:
                    x = x[:-1]
                    balances.append(x)
        #Gets the 7th element and every 7th element after that eg index 6,12...
        balances = balances[6::7]
        return balances

    def retrieveTransAccountNos(self):
        nums = []
        if path.exists("Transactions.data"):
            f = open("Transactions.data", "r")
            if f.mode == "r":
                data = f.readlines()
                for x in data:
                    x = x[:-1]
                    nums.append(x)
        #Gets the 2nd element and every 3rd element after that eg index 1,4...
        nums = nums[0::3]
        return nums

    def retrieveDebTransactions(self):
        transacs = []
        if path.exists("Transactions.data"):
            f = open("Transactions.data", "r")
            if f.mode == "r":
                data = f.readlines()
                for x in data:
                    x = x[:-1]
                    transacs.append(x)
        #Gets the 2nd element and every 3rd element after that eg index 1,4...
        transacs = transacs[1::3]
        return transacs

    def retrieveCreTransactions(self):
        transacs = []
        if path.exists("Transactions.data"):
            f = open("Transactions.data", "r")
            if f.mode == "r":
                data = f.readlines()
                for x in data:
                    x = x[:-1]
                    transacs.append(x)
        #Gets the 3nd element and every 3rd element after that eg index 2,5...
        transacs = transacs[2::3]
        return transacs

    def getSpecificDebTrans(self, accNo):
        nums = self.retrieveTransAccountNos()
        trans = self.retrieveDebTransactions()
        for i in range(0,len(nums)):
            if accNo == int(nums[i]):
                self.transactions.append(trans[i])
        print(self.transactions)

    def getUpdateDetails(self, balType):
        data = []
        with open("Other_Details.data", "r") as f:
            lines = f.readlines()
        for i in range(0,len(lines)):
            data.append(i)
        if balType == "Debit":
            data = data[4::7]
        elif balType == "Credit":
            data = data[6::7]
        return data,lines