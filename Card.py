import Account
import ctypes

class Card:
    acc = Account.Account()
    accountNos = acc.retrieveOtherData("AccNo")
    firstNames = acc.retrieveOtherData("FName")
    lastNames = acc.retrieveOtherData("LName")
    doBs = acc.retrieveOtherData("DoB")
    debBal = acc.retrieveOtherData("Debit")
    overdraft = acc.retrieveOtherData("Overdraft")
    creBal = acc.retrieveOtherData("Credit")
    transacs = acc.retrieveTransacData("Debit")
    accountNo = 0

    def getDebitBalance(self):
        return self.debBal[self.accountNo]

    def getOverdraft(self):
        return self.overdraft[self.accountNo]

    def getCreditBalance(self):
        return self.creBal[self.accountNo]

    def updateBalance(self, balType, amount):
        '''
        Will update the Debit or Credit balance in the file
        '''
        accNo = self.accountNo
        overdraft = int(self.getOverdraft())
        if balType == "Debit":
            data, lines = self.acc.getUpdateDetails("Debit")
            amount -= overdraft
            for i in range(0,len(data)):
                if i == accNo:
                    lines[data[i]] = str(amount) + '\n'
        elif balType == "Credit":
            data, lines = self.acc.getUpdateDetails("Credit")
            amount -= overdraft
            for i in range(0,len(data)):
                if i == accNo:
                    lines[data[i]] = str(amount) + '\n'

        with open("Other_Details.data", "w") as f:
            for line in lines:
                f.write(line)
            print("File Updated.")

    def editFunds(self, operator, balType, amount):
        '''
        Calculate new Debit/Credit balance
        '''
        debBal = int(self.getDebitBalance()) + int(self.getOverdraft())
        creBal = int(self.getCreditBalance()) + int(self.getOverdraft())
        if operator == "+":
            newDebBal = debBal + int(amount)
            newCreBal = creBal + int(amount)
        elif operator == "-":
            newDebBal = debBal - int(amount)
            newCreBal = creBal - int(amount)
            if newDebBal < 0:
                print("Not enough funds in Debit balance")
                exit()

        if balType == "Debit":
            self.acc.newTransaction(self.accountNo,newDebBal,creBal)
            self.updateBalance("Debit", newDebBal)
        elif balType == "Credit":
            self.acc.newTransaction(self.accountNo,debBal,newCreBal)
            self.updateBalance("Credit", newCreBal)

    def getOverallBalance(self, balType):
        '''
        Get the overall Debit/Credit balance (Debit/Credit + Overdraft)
        '''
        overallDebBal = int(self.getDebitBalance()) + int(self.getOverdraft())
        overallCreBal = int(self.getCreditBalance()) + int(self.getOverdraft())
        if balType == "Debit":
            print("Debit Balance: ")
            print('Bal: ', self.getDebitBalance(), ' | Overdraft: ', self.getOverdraft())
            print('Overall Balance: ', overallDebBal)
        elif balType == "Credit":
            print("Credit Balance: ")
            print('Bal: ', self.getCreditBalance(), ' | Overdraft: ', self.getOverdraft())
            print('Overall Balance: ', overallCreBal)