import numpy as np
import os.path
import re
import DynamicArray
import DoublyLinkedList
import time
from os import path

class Account:
    transactions = DynamicArray.DynamicArray()
    creTransactions = DoublyLinkedList.LinkedDeque()
    def __init__(self, accountNo=None, userName=None, password=None, fName=None, lName=None, doB=None, debitBalance=None, overdraft=None, creditBalance=None):
        self._accountNo = accountNo
        self._userName = userName
        self._password = password
        self._fName = fName
        self._lName = lName
        self._doB = doB
        self._debitBalance = debitBalance
        self._overdraft = overdraft
        self._creditBalance = creditBalance

    # Setter functions created for the Account details
    def setAccountNo(self, accountNo):
        self._accountNo = accountNo

    def setUserName(self, userName):
        self._userName = userName

    def setPassword(self, password):
        self._password = password

    def setFName(self, fName):
        self._fName = fName

    def setLName(self, lName):
        self._lName = lName

    def setDoB(self, doB):
        self._doB = doB

    def setDebitBalance(self, debitBalance):
        self._debitBalance = debitBalance

    def setOverdraft(self, overdraft):
        self._overdraft = overdraft

    def setCreditBalance(self, creditBalance):
        self._creditBalance = creditBalance

    # Getter functions created for the Account details
    def getAccountNo(self):
        return self._accountNo

    def getUserName(self):
        return self._userName

    def getPassword(self):
        return self._password

    def getFName(self):
        return self._fName

    def getLName(self):
        return self._lName

    def getDoB(self):
        return self._doB

    def getDebitBalance(self):
        return self._debitBalance

    def getOverdraft(self):
        return self._overdraft

    def getCreditBalance(self):
        return self._creditBalance

    # Adds login details to the file "Login_Data.data"
    def loginDatabase(self):
        '''
        Adds data to the 'Login_Data.data' file
        '''
        usern = self.getUserName()
        passw = self.getPassword()
        f = open("Login_Data.data", "a+")
        f.write(usern + "\n")
        f.write(passw + "\n")
        f.close()

    # Add other details to the file "Other_Details.data"
    def otherDatabase(self):
        '''
        Adds data to the 'Other_Details.data' file
        '''
        accNum = self.getAccountNo()
        fiName = self.getFName()
        laName = self.getLName()
        dob = self.getDoB()
        debitBal = self.getDebitBalance()
        overdraft = self.getOverdraft()
        creditBal = self.getCreditBalance()

        f = open("Other_Details.data", "a+")
        f.write(accNum + "\n")
        f.write(fiName + "\n")
        f.write(laName + "\n")
        f.write(dob + "\n")
        f.write(debitBal + "\n")
        f.write(overdraft + "\n")
        f.write(creditBal + "\n")
        f.close()

    def transacDatabase(self):
        '''
        Adds Debit and Credit balances to the 'Transactions.data' file
        '''
        accNum = self.getAccountNo()
        debitBal = self.getDebitBalance()
        overdraft = self.getOverdraft()
        creditBal = self.getCreditBalance()
        totalDebAmount = int(debitBal) + int(overdraft)
        totalCreAmount = int(creditBal) + int(overdraft)

        f = open("Transactions.data", "a+")
        f.write(str(accNum) + "\n")
        f.write(str(totalDebAmount) + "\n")
        f.write(str(totalCreAmount)+ "\n")
        f.close()

    def newTransaction(self, accNo, debAmount, creAmount):
        f = open("Transactions.data", "a+")
        f.write(str(accNo) + "\n")
        f.write(str(debAmount) + "\n")
        f.write(str(creAmount) + "\n")
        f.close()

    # Get either the users or passwords from the Login_Data depending on parameter
    def retrieveLoginData(self, want):
        '''
        Retrieve data from the 'Login.data' file
        '''
        finalData = []
        if path.exists("Login_Data.data"):
            f = open("Login_Data.data", "r")
            if f.mode == 'r':
                data = f.readlines()
                for x in data:
                    x = x[:-1]
                    finalData.append(x)
        if want == 'Users':
            finalData = finalData[0::2]
        elif want == 'Passwords':
            finalData = finalData[1::2]
        return finalData

    def retrieveOtherData(self, want):
        '''
        Retrieve data from the 'Other_Details.data' file
        '''
        finalData = []
        if path.exists("Other_Details.data"):
            f = open("Other_Details.data", "r")
            if f.mode == 'r':
                data = f.readlines()
                for x in data:
                    x = x[:-1]
                    finalData.append(x)
        if want == 'AccNo':
            finalData = finalData[0::7]
        elif want == 'FName':
            finalData = finalData[1::7]
        elif want == 'LName':
            finalData = finalData[2::7]
        elif want == 'DoB':
            finalData = finalData[3::7]
        elif want == 'Debit':
            finalData = finalData[4::7]
        elif want == 'Overdraft':
            finalData = finalData[5::7]
        elif want == 'Credit':
            finalData = finalData[6::7]
        return finalData

    def retrieveTransacData(self, want):
        '''
        Retrieve data from the 'Transactions.data' file
        '''
        finalData = []
        if path.exists("Transactions.data"):
            f = open("Transactions.data", "r")
            if f.mode == 'r':
                data = f.readlines()
                for x in data:
                    x = x[:-1]
                    finalData.append(x)
        if want == 'AccNo':
            finalData = finalData[0::3]
        elif want == 'Debit':
            finalData = finalData[1::3]
        elif want == 'Credit':
            finalData = finalData[2::3]
        return finalData

    def addDebTransactions(self, accNo):
        '''
        Add a new Debit transaction to the DynamicArray
        '''
        nums = self.retrieveTransacData("AccNo")
        trans = self.retrieveTransacData("Debit")
        for i in range(0,len(nums)):
            if accNo == int(nums[i]):
                self.transactions.append(trans[i])

    def addCreTransactions(self, accNo):
        '''
        Add a new Credit transaction to the LinkedList
        '''
        nums = self.retrieveTransacData("AccNo")
        trans = self.retrieveTransacData("Credit")
        for i in range(0, len(nums)):
            if accNo == int(nums[i]):
                self.creTransactions.insert_last(trans[i])
        
    def getTrans(self, balType):
        '''
        Print either the Debit or Credit transactions from the
        DynamicArray or the DoublyLinkedList
        '''
        if balType == "Debit":
            print(self.transactions)
        elif balType == "Credit":
            print(self.creTransactions)

    def addFirstTrans(self, balType, value):
        '''
        Insert Debit/Credit balance to DynamicArray or LinkedList
        '''
        print("Value Added")
        if balType == "Debit":
            self.transactions.add_to_front(value)
            print(self.transactions)
        elif balType == "Credit":
            self.creTransactions.insert_first(value)
            print(self.creTransactions)

    def insertTrans(self, i, value):
        '''
        Insert item into DynamicArray
        '''
        self.transactions.insert(int(i), int(value))
        print("Value Added")
        print(self.transactions)

    def removeFirstTrans(self, balType):
        '''
        Removes the first transaction for Debit or Credit
        '''
        if balType == "Debit":
            self.transactions.remove_first()
            print(self.transactions)
        elif balType == "Credit":
            self.creTransactions.delete_first()
            print(self.creTransactions)
        print("Deletion Complete")

    def removeLastTrans(self, balType):
        '''
        Removes last transaction from Debit or Credit
        '''
        if balType == "Debit":
            self.transactions.pop()
            print(self.transactions)
        elif balType == "Credit":
            self.creTransactions.delete_last()
            print(self.creTransactions)
        print("Deletion Complete")

    def itemExist(self, balType, value):
        '''
        Check if the Debit/Credit transaction exists in the Array/List
        '''
        if balType == "Debit":
            print(self.transactions.doesItemExist(str(value)))
        elif balType == "Credit":
            print(self.creTransactions.does_ele_exist(str(value)))
        
    def getItem(self, balType, i):
        '''
        Gets a Debit/Credit transaction from a certain index
        '''
        if balType == "Debit":
            print(self.transactions[i])
        elif balType == "Credit":
            print(self.creTransactions.find_element(i))

    def addLastCreTransac(self, value):
        '''
        Adds Credit transaction to end of liked list
        '''
        self.creTransactions.insert_last(value)
        print("Value Added")
        print(self.creTransactions)

    def beginningInsertTest(self, balType):
        '''
        Time/Space Complexity Test
        Test the time it takes to insert n number of records at the beginning into either data structure
        and test the capacity (size) of the data structures
        '''
        start_time = time.time()
        if balType == "Debit":
            print('Array Capacity Before Test: ', self.transactions.getSize())
            for i in range(10000):
                self.transactions.add_to_front(i)
            print(self.transactions)
            print('Array Capacity After Test: ', self.transactions.getSize())
            new_time = float(time.time()) - float(start_time)
            print('---- %s seconds ----' %new_time)
        elif balType == "Credit":
            print('Array Capacity Before Test: ', len(self.creTransactions))
            for i in range(10000):
                self.creTransactions.insert_first(i)
            print(self.creTransactions)
            print('Array Capacity After Test: ', len(self.creTransactions))
            new_time = float(time.time()) - float(start_time)
            print('---- %s seconds ----' %new_time)

    def endInsertTest(self, balType):
        '''
        Test the time it takes to insert n number of records at the end into either data structure
        and test the capacity (size) of the data structures
        '''
        start_time = time.time()
        if balType == "Debit":
            print('Array capacity Before Test: ', self.transactions.getSize())
            for i in range(10000):
                self.transactions.append(i)
            print(self.transactions)
            print('Array Capacity After Test: ', self.transactions.getSize())
            new_time = float(time.time()) - float(start_time)
            print('---- %s seconds ----' %new_time)
        elif balType == "Credit":
            print('Array capacity Before Test: ', len(self.creTransactions))
            for i in range(10000):
                self.creTransactions.insert_last(i)
            print(self.creTransactions)
            print('Array capacity After Test: ', len(self.creTransactions))
            new_time = float(time.time()) - float(start_time)
            print('---- %s seconds ----' %new_time)

    def getUpdateDetails(self, balType):
        '''
        Gets Debit/Credit balance to update
        '''
        data = []
        with open("Other_Details.data", "r") as f:
            lines = f.readlines()
        for i in range(0,len(lines)):
            data.append(i)
        if balType == "Debit":
            data = data[4::7]
        elif balType == "Credit":
            data = data[6::7]
        return data,lines