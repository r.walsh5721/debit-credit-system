This was a Project that was created for a Module in my fourth year of university\n
The name of the Module was called Programming Languages: Constructs and Data Structures\n
The main purpose of this project was to use and compare the two data structures used (DynamicArray, LinkedList)\n
The Time Complexity of some of the functions were compared as they have similar functions but the LinkedList is more efficient than the Array\n
The Space Complexity was also compared between both of the data structures\n
When first running the software, three .data files will be created which will store data from the software ('Login_Data.data', 'Other_Details.data', 'Transactions.data')