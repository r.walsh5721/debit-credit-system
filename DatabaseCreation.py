import pandas as pd
import Test
from openpyxl import load_workbook

# https://github.com/PyCQA/pylint/issues/3060 pylint: disable=abstract-class-instantiated

class Database:


    def createDatabase(self):
        # Create a Pandas Excel writer using XlsxWriter as the engine.
        writer = pd.ExcelWriter('AccountDetails.xlsx', engine='xlsxwriter')

        # dataframe Account columns
        accountDF = pd.DataFrame({
                        'UserName' : [],
                        'Password' : [],
                        'FirstName' : [],
                        'LastName' : [],
                        'DoB' : [],
                        'DebitBal' : [],
                        'OverDraft' : [],
                        'CreditBal' : []})

        transacDF = pd.DataFrame({
                        'DebitBal' : [],
                        'OverDraft' : [],
                        'CreditBal' : []})

        # Convert the dataframe to an XlsxWriter Excel object.
        accountDF.to_excel(writer, sheet_name='AccountTable', index=True)
        transacDF.to_excel(writer, sheet_name='TransacTable', index=True)

        writer.save()

    def appendToAccountTable(self, user, password, fName, lName, dob, debBal, overdraft, creBal):
        # new dataframe with same columns
        df = pd.DataFrame({
                        'UserName' : [user],
                        'Password' : [password],
                        'FirstName' : [fName],
                        'LastName' : [lName],
                        'DoB' : [dob],
                        'DebitBal' : [debBal],
                        'OverDraft' : [overdraft],
                        'CreditBal' : [creBal]})

        writer = pd.ExcelWriter('AccountDetails.xlsx', engine='openpyxl')
        # try to open an existing workbook
        writer.book = load_workbook('AccountDetails.xlsx')
        # copy existing sheets
        writer.sheets = dict((ws.title, ws) for ws in writer.book.worksheets)
        # read existing file
        reader = pd.read_excel(r'AccountDetails.xlsx')
        # write out the new sheet
        df.to_excel(writer,sheet_name='AccountTable',index=True,header=False,startrow=len(reader)+1)

        writer.close()

if __name__ == "__main__":
    d = Database()
    #d.createDatabase()
    d.appendToAccountTable('Richo','1','Richard','Walsh','13/11/1997','1000','750','1000')